package jwt.constants;

/**
 * Klasa trzymająca stałe wykorzystywane w aplikacji
 */
public class Constants {
    public static final String AUTHENTICATED_USER = "Hello authenticated user!. Your username is ";
    public static final String PERMISSION = " and your permission is ";
    public static final String PASSWORD = "test123";
    public static final String HEADER_NAME = "Content-Type";
    public static final String HEADER_VALUE = "text/html; charset=UTF-8";
}
