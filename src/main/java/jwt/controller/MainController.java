package jwt.controller;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import jwt.constants.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Głowny kotroler aplikacji
 */
@RestController
public class MainController {
    private final Logger logger = LoggerFactory.getLogger(MainController.class);
    @Value("jwt.secret.key")
    private String secretKey;

    @GetMapping(value = "/authenticated", produces = "text/plain")
    public String authenicated(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Cookie[] cookies = request.getCookies();
        String token = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("token")) {
                    token = cookie.getValue();
                }
            }
        }

        if (token == null) {
            response.sendRedirect("/login.html");
        } else {
            Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
            String username = claims.getSubject();
            String role = claims.get("roles").toString();
            if (logger.isInfoEnabled()) {
                logger.info(String.format("%s%s%s%s", Constants.AUTHENTICATED_USER, username, Constants.PERMISSION, role));
            }

            return Constants.AUTHENTICATED_USER + username + Constants.PERMISSION + role;
        }
        return "";
    }
}
