package jwt.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Klasa kontrolera wylogowującego użytkownika
 */
@RestController
public class LogoutController {
    private final Logger logger = LoggerFactory.getLogger(LogoutController.class);

    @GetMapping({"/logout"})
    public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (request.getCookies() == null) {
            response.sendRedirect("/index.html");
        } else {
            Cookie cookie = new Cookie("token", "");
            cookie.setMaxAge(0);
            response.addCookie(cookie);
            response.sendRedirect("/index.html");
            logger.info("Nastąpiło wylogowanie z systemu. ");
        }

    }
}
