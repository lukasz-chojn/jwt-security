package jwt.controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * Kontroler logujący użytkownika i zwracający token
 */
@RestController
public class AuthenticationController {

    private final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Value("jwt.secret.key")
    private String secretKey;

    @PostMapping("/authenticate")
    public ResponseEntity authenticate(HttpServletRequest request, HttpServletResponse response) {
        final int expirationTime = 60000;
        authenticate(request.getParameter("username"), request.getParameter("password"));
        String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();

        String token = Jwts.builder()
                .setSubject(request.getParameter("username"))
                .claim("roles", role)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + expirationTime))
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
        response.addCookie(new Cookie("token", token));
        if (logger.isInfoEnabled()) {
            logger.info("Użytkownik zalogowany");
        }
        return ResponseEntity.ok(token);
    }

    private void authenticate(String username, String password) {
        final Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
