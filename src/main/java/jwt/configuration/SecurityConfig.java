package jwt.configuration;

import jwt.constants.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Klasa konfiguracyjna zabezpieczeń aplikacji
 */
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final Logger logger = LoggerFactory.getLogger(SecurityConfig.class);

    protected void configure(HttpSecurity http) throws Exception {
        logger.info("Konfiguracja zabezpieczeń ");
        http.authorizeRequests()
                .antMatchers("/index.html").permitAll()
                .antMatchers("/login.html").permitAll()
                .anyRequest().authenticated()
                .and()
                .cors().disable()
                .csrf().disable();
    }

    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        logger.info("Konfiguracja użytkowników ");
        auth.inMemoryAuthentication()
                .withUser("user").password(passwordEncoder().encode(Constants.PASSWORD)).roles("USER")
                .and()
                .withUser("admin").password(passwordEncoder().encode(Constants.PASSWORD)).roles("ADMIN")
                .and()
                .withUser("anonymuous").password(passwordEncoder().encode(Constants.PASSWORD)).roles("ANONYMUOUS");
    }

    public void configure(WebSecurity web) {
        logger.info("Konfiguracja filtra dla plików js i css");
        web.ignoring()
                .antMatchers("/js/**", "/css/**").anyRequest();
    }

    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        if (logger.isInfoEnabled()) {
            logger.info(String.format("Konfiguracja %s", AuthenticationManager.class.getSimpleName()));
        }
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        if (logger.isInfoEnabled()) {
            logger.info(String.format("Podnoszę bean'a %s", BCryptPasswordEncoder.class.getSimpleName()));
        }
        return new BCryptPasswordEncoder();
    }
}
