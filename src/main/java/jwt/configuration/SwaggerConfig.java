package jwt.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Klasa konfiguracyjna do obsługi Swaggera
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private final Logger logger = LoggerFactory.getLogger(SwaggerConfig.class);

    @Bean
    public Docket api() {
        if (logger.isInfoEnabled()) {
            logger.info(String.format("Uruchomiono Swaggera z poziomu klasy %s", SwaggerConfig.class.getSimpleName()));
        }
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("jwt.controller"))
                .paths(PathSelectors.regex("/.*"))
                .build().apiInfo(about());

    }

    private ApiInfo about() {
        return new ApiInfoBuilder()
                .title("JWT app")
                .description("This is educational Java app. This application has been written in java 8 language and spring " +
                        "framework. Security in app is based on Spring Security and JWT.")
                .version("2.0")
                .build();
    }
}
