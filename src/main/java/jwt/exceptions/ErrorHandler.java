package jwt.exceptions;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import jwt.constants.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.MessageFormat;

/**
 * Kontroler przechwytujący błędy
 */
@RestControllerAdvice
public class ErrorHandler extends RuntimeException {

    private final Logger logger = LoggerFactory.getLogger(ErrorHandler.class);

    public ErrorHandler() {
        if (logger.isInfoEnabled()) {
            logger.info(String.format("Startująca klasa %s", ErrorHandler.class.getSimpleName()));
        }
    }

    @ExceptionHandler(SignatureException.class)
    public ResponseEntity<String> bladSygnaturyTokena(Exception e) {
        if (logger.isInfoEnabled()) {
            logger.error(MessageFormat.format("Błąd przy przetwarzaniu tokena {0}", e.getLocalizedMessage()));
        }
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .header(Constants.HEADER_NAME, Constants.HEADER_VALUE)
                .body("Błąd przy przetwarzaniu tokena " + e.getLocalizedMessage());
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<String> bladuprawnien() {
        if (logger.isInfoEnabled()) {
            logger.error("Błędny login lub hasło");
        }
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .header(Constants.HEADER_NAME, Constants.HEADER_VALUE)
                .body("Błędny login lub hasło");
    }

    @ExceptionHandler(ExpiredJwtException.class)
    public ResponseEntity<String> bladWaznosciTokena(HttpServletResponse response, Exception e) throws IOException {
        if (logger.isInfoEnabled()) {
            logger.error(MessageFormat.format("Token wygasł {0}", e.getLocalizedMessage()));
        }
        Cookie cookie = new Cookie("token", "");
        cookie.setMaxAge(0);
        response.sendRedirect("/login.html");
        response.addCookie(cookie);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .header(Constants.HEADER_NAME, Constants.HEADER_VALUE)
                .body("Token wygasł " + e.getLocalizedMessage());
    }
}
