package jwt.controller;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.Cookie;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.testng.Assert.assertEquals;

public class MainControllerTest {

    private MockMvc mockMvc;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private Authentication authentication;
    @Mock
    private SecurityContext securityContext;
    @Mock
    private Cookie cookie;
    @InjectMocks
    private MainController mainController;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();
    }

    @Test
    public void testAuthenicated() throws Exception {

        Cookie cookie = cookie();

        ReflectionTestUtils.setField(mainController, "secretKey", "jwtSecret");

        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        MockHttpServletRequestBuilder requestBuilder = get("/authenticated");
        requestBuilder.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        requestBuilder.header(HttpHeaders.AUTHORIZATION, cookie.getValue());

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getStatus(), 302);
    }

    private Cookie cookie() {
        return new Cookie("token", "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjoiW1JPTEVfQURNSU5dIiwiaWF0IjoxNTg3MzI2ODQ1LCJleHAiOjE1ODczMjY5MDV9.bvZ9Spb_K3H11Wlrht_yffaflNuSZSOSbiYcOVRrpyE");
    }
}