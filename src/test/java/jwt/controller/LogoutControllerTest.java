package jwt.controller;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.Cookie;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.testng.Assert.assertEquals;

public class LogoutControllerTest {

    private MockMvc mockMvc;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private Authentication authentication;
    @Mock
    private SecurityContext securityContext;
    @Mock
    private Cookie cookie;
    @InjectMocks
    private LogoutController logoutController;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(logoutController).build();
    }

    @Test
    public void testLogout() throws Exception {

        when(cookie.getName()).thenReturn("token");

        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        MockHttpServletRequestBuilder requestBuilder = get("/logout");

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getStatus(), 302);
    }
}